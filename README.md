# Ccigit

## Steps for running on linux/windows

Note: Requires `nodejs` to be installed
1. Open console/command prompt
2. Navigate to the folder with this project
3. Run `npm install` to install all packages
4. Run `ng serve` for a dev server. 
5. Navigate to `http://localhost:4200/`.

## Limitations of project

* Issues Search only returns maximum of 100 issues.  This app is only working with the most recent 100 issues.  In a production app pagination would be implemented.
* Due to the time required, some features were not fully implemented:
  * An NGRX store was not implemented, instead a very basic Observable pattern service was created to simulate a store.
  * Error handling on the API was not implemented.  It will catch errors to prevent a crash but will not show a snackbar to the user
  * Some material scaffolding was not implemented, e.g. the header does not have a backarrow or related animation
  * A router has been configured so that the url will match what the url would look like in Git.  e.g. `localhost:4200/repos/twbs/bootstrap/details`.  This has not been tested as to whether there are special characters on some repos that might break it.
  * The app does not handle refreshing while on a repository details page.
  * No loaders have been implemented at this point.  Ideally I would implement a targeted loader on the search results and a resolver-related loader on the details page
  

## Notable code

* `StoreService` - I have used an observable pattern that I like here.  It allows for a `private` observable that the service can emit on, while exposing a read-only version of the observable for anything else.
* `StoreService` - The observables use `ShareReplay` which is extremely important and few angular devs seem to understand correctly.  When listening to an observable, especially if that observable has a pipe, all code in the pipe will run per subscriber when an event fires.  so 3 subsribers = 3 x the code runs per event.  ShareReplay will run the code once and emit the final result 3 times to each listener.
* `search-results.component.ts` and `issues.component.ts` - The search and the filters are using a `Reactive Form`.  This has a number of benefits.  
  * It allows one to easily expand the filters without littering the component with variables.  
  * It allows one to easily subscribe to changes and update other parts of the component
  * It allows one to easily insert default values or cater for filters being remembered by either nxrg or on the api side.
* CSS
  * In `search-results.component.css` I have used my preferred naming convention for CSS classes, instead of naming `title` and `description` I prefer to name classes as `search-title` and `search-description`.  This prevents future agony when another element is added that is also a title or description.  I have not implemented this throughout this test, but I did wish to include it in at least 1 component.
  * I have used a mix of flex-box and css-grid to show my knowledge of both.
  * I like to nest CSS (in SCSS), but only where it makes sense, i.e. only elements that are guaranteed to be nested.  You will find some selectors more advanced than just targeting classes.  This is second-nature to me, something I do frequently (while trying to keep them simple enough that it doesn't overwhelm/confuse junior devs).
  * A custom material theme has been defined in `styles.scss`


## Feedback on test

The test is a lot of work.  All in all, over about 3 days, it required nearly 7 hours to complete to this stage.

I highly recommend slimming it down considerably.  I would recommend the following changes:
1. Ideally a test should be less than 3 hours work.
2. Limit the test to a single page.  Perhaps the issues page as this incorporates api, filters and a graph.  Perhaps the page for searching repos could already be built, and they must tie the two together.
3. Providing a basic already setup project.  I spent approximately 3 hours setting up: Angular project, material scaffold, api service, defining interfaces for api calls, setting up a basic store to emulate ngrx.  
4. Have all API interfaces defined already.  There is not much to be learned from a candidate recreating these.
5. Provide a design mockup.  This allows for testing whether a candidate has the apitude to match a design and how good they are at details.