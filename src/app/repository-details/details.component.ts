import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { concatMap, map, Subscription } from 'rxjs';
import { RepositoryResult, SearchResult } from 'src/interfaces';
import { StoreService } from 'src/services/store.service';

@Component({
  selector: 'details-root',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnDestroy, OnInit {

  public readonly repo$ = this.getRepo();
  public readonly openIssues$= this.getOpenIssues();
  private subscriptions: Subscription[] = [];


  constructor(
    private activatedRoute: ActivatedRoute,
    private storeService: StoreService,
  ) {

  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe()); // Cleanup any subscriptions
  }

  private getRepo() {
    return this.activatedRoute.params.pipe(
      concatMap(params => {
        return this.storeService.searchResults.pipe(
          map(repos => repos?.items.find(i => i.full_name == params['owner'] + '/' + params['repo']))
        )
      })
    );
  }

  private getOpenIssues() {
    return this.storeService.issueCountOpen;
  }



}
