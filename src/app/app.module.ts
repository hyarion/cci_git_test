import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSelectModule } from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';
import { ApiService } from 'src/services/api.service';
import { StoreService } from 'src/services/store.service';
import { SearchResultsComponent } from './search-results/search-results.component';
import { DetailsComponent } from './repository-details/details.component';
import { IssuesComponent } from 'src/app/repository-issues/issues.component';
import { RepositoryNavComponent } from 'src/app/repositor-nav/repository-nav.component';
import { IssuesResolver } from 'src/app/repository-issues/issues.resolver';

const routes: Routes = [
  {
    path: 'search',
    component: SearchResultsComponent,
  },
  {
    path: 'repo/:owner/:repo',
    component: RepositoryNavComponent,
    children: [
      {
        path: 'details',
        component: DetailsComponent,
      },
      {
        path: 'issues',
        component: IssuesComponent,
        resolve: [IssuesResolver],
      },
      {
        path: '**',
        redirectTo: 'details'
      }
    ]
  },
  {
    path: '**',
    redirectTo: 'search',
  }
];

@NgModule({
  declarations: [
    AppComponent,
    SearchResultsComponent,
    DetailsComponent,
    IssuesComponent,
    RepositoryNavComponent,
  ],
  imports: [
    BrowserModule,
    // make parent parameters available to children
    RouterModule.forRoot(routes, { paramsInheritanceStrategy: 'always' }), 
    HttpClientModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    ReactiveFormsModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatTabsModule,
    MatSelectModule,
  ],
  providers: [
    ApiService,
    StoreService,
    IssuesResolver,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
