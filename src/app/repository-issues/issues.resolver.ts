import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { concatMap, forkJoin, map, of, tap } from 'rxjs';
import { ApiService } from 'src/services/api.service';
import { StoreService } from 'src/services/store.service';

@Injectable()
export class IssuesResolver implements Resolve<any> {
  constructor(
    private apiService: ApiService,
    private storeService: StoreService,
  ) {

  }
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    // return of(true);
    return forkJoin([
      this.apiService.fetchLatestIssues(route.params['owner'], route.params['repo']).pipe(
        tap(issues => this.storeService.dispatchIssuesResult(issues))
      ),
      this.apiService.fetchIssueCountOpen(route.params['owner'], route.params['repo']).pipe(
        tap(issues => this.storeService.dispatchIssueCountOpen(issues ? issues.total_count : 0))
      ),
      this.apiService.fetchIssueCountClosed(route.params['owner'], route.params['repo']).pipe(
        tap(issues => this.storeService.dispatchIssueCountClosed(issues ? issues.total_count : 0))
      ),
    ])
    
  }

  

}