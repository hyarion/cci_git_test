import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ArcElement, Chart, ChartConfiguration, Legend, PieController, SubTitle, Title, Tooltip } from 'chart.js';
import { combineLatest, map, startWith, Subscription, take } from 'rxjs';
import { StoreService } from 'src/services/store.service';

Chart.register(
  ArcElement,
  PieController,
  Legend,
  Title,
  Tooltip,
  SubTitle
);

@Component({
  selector: 'issues-root',
  templateUrl: './issues.component.html',
  styleUrls: ['./issues.component.scss']
})
export class IssuesComponent implements OnDestroy, OnInit, AfterViewInit {

  @ViewChild('chart')
  readonly chartElement!: ElementRef<HTMLCanvasElement>;

  public filterForm = this.createFilterForm();
  public readonly issues$ = this.getIssues();

  private subscriptions: Subscription[] = [];
  private chart!: Chart;
  private chartConfiguration!: ChartConfiguration;

  constructor(
    private storeService: StoreService,
    private formbuilder: FormBuilder,
  ) {

  }
  ngAfterViewInit(): void {
    this.setupChart();
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe()); // Cleanup any subscriptions
  }

  private getIssues() {
    return combineLatest([
      this.storeService.issueResults,
      this.filterForm.valueChanges.pipe(startWith(this.filterForm.value)),
    ]).pipe(
      map(([issuesResult, filters]) => {
        if (issuesResult == null) { return []; }
        return issuesResult.items.filter(i => filters.state === '' || filters.state == i.state)
      }),
    )
  }

  private createFilterForm() {
    const form = this.formbuilder.group({
      state: ['', []]
    })
    return form;
  }

  private setupChart() {
    combineLatest([
      this.storeService.issueCountOpen.pipe(take(1)),
      this.storeService.issueCountClosed.pipe(take(1)),
    ])
      .subscribe(([open, closed]) => {
        this.chartConfiguration = {
          type: 'pie',
          data: {
            labels: ['Open', 'Closed'],
            datasets: [{
              label: 'Open vs Closed Issues',
              data: [open, closed],
              backgroundColor: [
                '#FF6384',
                '#36A2EB',
              ]
            }]
          },
          options: {
            plugins: {
              legend: {
                position: 'top',
              },
              title: {
                display: true,
                text: 'Open vs Closed Issues'
              }
            }
          }
        }

        this.chart = new Chart(this.chartElement.nativeElement, this.chartConfiguration);
      })
  }

}

