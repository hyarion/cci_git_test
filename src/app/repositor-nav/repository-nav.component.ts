import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { concatMap, map, Subscription } from 'rxjs';
import { StoreService } from 'src/services/store.service';

@Component({
  selector: 'repository-nav-root',
  templateUrl: './repository-nav.component.html',
  styleUrls: ['./repository-nav.component.scss']
})
export class RepositoryNavComponent implements OnDestroy, OnInit {
  public activeLink: String = 'details';
  private subscriptions: Subscription[] = [];

  constructor(
    private activatedRoute: ActivatedRoute,
    private storeService: StoreService,
  ) {

  }

  ngOnInit(): void {

  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe()); // Cleanup any subscriptions
  }


}
