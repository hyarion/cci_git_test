import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { concatMap, debounceTime, Subscription, tap } from 'rxjs';
import { ApiService } from 'src/services/api.service';
import { StoreService } from 'src/services/store.service';

@Component({
  selector: 'search-results-root',
  templateUrl: './search-results.component.html',
  styleUrls: ['./search-results.component.scss']
})
export class SearchResultsComponent implements OnDestroy, OnInit {

  public searchForm = this.createSearchForm();
  public searchResults$ = this.storeService.searchResults;
  private subscriptions: Subscription[] = [];


  constructor(
    private formbuilder: FormBuilder,
    private apiService: ApiService,
    private storeService: StoreService,
  ) {

  }

  ngOnInit(): void {
    this.subscriptions.push(
      // Note: In a real-world project I would implement a solution for the form to be Typed
      this.searchForm.controls['search'].valueChanges.pipe(
        debounceTime(300), // prevent rapid overfetching from API
        concatMap(term => this.apiService.searchRepos(term)),
        tap(result => this.storeService.dispatchSearchResult(result)),
      ).subscribe(),
    );
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe()); // Cleanup any subscriptions
  }

  private createSearchForm() {
    const form = this.formbuilder.group({
      search: ['', []]
    })
    return form;
  }


}
