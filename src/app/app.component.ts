import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { Router } from '@angular/router';
import { concatMap, debounceTime, Subscription, tap } from 'rxjs';
import { ApiService } from 'src/services/api.service';
import { StoreService } from 'src/services/store.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnDestroy, OnInit {

  private subscriptions: Subscription[] = [];

  constructor(
    private storeService: StoreService,
    private router: Router,
  ) {

  }

  ngOnInit(): void {
    
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe()); // Cleanup any subscriptions
  }

  public navHome() {
    this.router.navigate(['/']);
  }


}
