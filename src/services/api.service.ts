import { Injectable, Injector } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError, of } from 'rxjs';
import { IssuesResult, SearchResult } from 'src/interfaces';

const ENDPOINTS = {
  searchRepos: 'https://api.github.com/search/repositories?q=',
  listIssues: 'https://api.github.com/search/issues?q=type:issue+repo:', 
  issueCountOpen: 'https://api.github.com/search/issues?per_page=1&q=type:issue+state:open+repo:',
  issueCountClosed: 'https://api.github.com/search/issues?per_page=1&q=type:issue+state:open+repo:',
}

@Injectable()
export class ApiService {
  
  constructor(
    private httpClient: HttpClient
  ) {
    
  }

  public searchRepos(term: String) {
    return this.httpClient.get<SearchResult>(ENDPOINTS.searchRepos + term).pipe(
      // Catch errors and show snackbar rather than crashing app
      catchError((err, caught) => {
        // TODO: Show snackbar
        return of(null);
      })
    )
  }

  public fetchLatestIssues(owner: String, repoName: String) {
    return this.httpClient.get<IssuesResult>(ENDPOINTS.listIssues + owner + '/' + repoName).pipe(
      catchError((err, caught) => {
        // TODO: Show snackbar
        return of(null);
      })
    )
  }

  public fetchIssueCountOpen(owner: String, repoName: String) {
    return this.httpClient.get<IssuesResult>(ENDPOINTS.issueCountOpen + owner + '/' + repoName).pipe(
      catchError((err, caught) => {
        // TODO: Show snackbar
        return of(null);
      })
    )
  }

  public fetchIssueCountClosed(owner: String, repoName: String) {
    return this.httpClient.get<IssuesResult>(ENDPOINTS.issueCountClosed + owner + '/' + repoName).pipe(
      catchError((err, caught) => {
        // TODO: Show snackbar
        return of(null);
      })
    )
  }
  
}