import { Injectable } from '@angular/core';
import { BehaviorSubject, shareReplay } from 'rxjs';
import { environment } from 'src/environments/environment';
import { IssuesResult, SearchResult } from 'src/interfaces';
import { searchData } from 'src/tempdata';
import { issuesData } from 'src/tempdata_issues';


/**
 * This is a makeshift service to act as a store.
 * In a real-world app I would implement NGRX instead.
 */
@Injectable()
export class StoreService {

  /**
   * This pattern ensures that emissions on the observable can only happen internally under controlled scenarios.
   * It also ensures a shareReplay is enforced on all subscribers.  
   * ShareReplay prevents unnecessary running of an observable.
   * 
   */
  private _searchResults = new BehaviorSubject<SearchResult | null>(environment.testData ? searchData : null);
  public searchResults = this._searchResults.pipe(shareReplay({ refCount: true, bufferSize: 1 }));

  private _issueResults = new BehaviorSubject<IssuesResult | null>(environment.testData ? issuesData : null);
  public issueResults = this._issueResults.pipe(shareReplay({ refCount: true, bufferSize: 1 }));

  private _issueCountOpen = new BehaviorSubject<number>(environment.testData ? 1 : 0);
  public issueCountOpen = this._issueCountOpen.pipe(shareReplay({ refCount: true, bufferSize: 1 }));
  private _issueCountClosed = new BehaviorSubject<number>(environment.testData ? 3 : 0);
  public issueCountClosed = this._issueCountClosed.pipe(shareReplay({ refCount: true, bufferSize: 1 }));

  constructor(

  ) {

  }

  public dispatchSearchResult(result: SearchResult | null) {
    this._searchResults.next(result);
  }

  public dispatchIssuesResult(result: IssuesResult | null) {
    this._issueResults.next(result);
  }

  public dispatchIssueCountOpen(result: number) {
    this._issueCountOpen.next(result);
  }

  public dispatchIssueCountClosed(result: number) {
    this._issueCountClosed.next(result);
  }
}