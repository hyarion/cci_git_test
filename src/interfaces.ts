
export interface SearchResult {
  total_count: number;
  incomplete_results: boolean;
  items: RepositoryResult[];
  [x: string | number | symbol]: unknown; // only defining what we're using - real-world app I would define every possible field.
}

export interface RepoOwner {
  login: string;
  id: number;
  node_id: string;
  avatar_url: string;
  url: string;
  type: string;
  site_admin: boolean;
  [x: string | number | symbol]: unknown;
}

export interface RepoLicense {
  key: string;
  name: string;
  spdx_id: string;
  url: string;
  node_id: string;
}

export interface RepositoryResult {
  id: number;
  node_id: string;
  name: string;
  full_name: string;
  private: boolean;
  owner: RepoOwner;
  html_url: string;
  description: string | null;
  fork: boolean;
  homepage: string | null;
  size: number;
  stargazers_count: number;
  watchers_count: number;
  language: string | null;
  has_issues: boolean;
  has_projects: boolean;
  has_wiki: boolean;
  has_pages: boolean;
  forks_count: number;
  open_issues_count: number;
  license: RepoLicense | null;
  topics: string[];
  forks: number;
  watchers: number;
  open_issues: number;
  default_branch: string;
  score: number;
  [x: string | number | symbol]: unknown;
}


// Issues
export interface IssuesResult {
  total_count: number;
  incomplete_results: boolean;
  items: IssueResult[];
  [x: string | number | symbol]: unknown;
}

export interface IssueResult {
  url: string;
  id: number;
  number: number;
  title: string;
  user: RepoOwner;
  state: string;
  locked: boolean;
  comments: number;
  created_at: string;
  updated_at: string;
  closed_at: string | null;
  body: string | null;
  [x: string | number | symbol]: unknown; 
}

export interface IssueLabel {
  id: number;
  node_id: string;
  url: string;
  name: string;
  color: string;
  default: boolean;
  description?: string;
}

